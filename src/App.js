import React from 'react';
import './App.css';
import Numbers from "./Numbers/Numbers";

class App extends React.Component {
    state = {
        newArray: []
    };

    randomNumber = (min, max) => {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    };

    getNumbers = () => {
        let numbersArray = [];

        while (numbersArray.length < 5) {
            const newNumber = this.randomNumber(5, 36);

            if (numbersArray.includes(newNumber)) {
                continue;
            } else {
                numbersArray.push(newNumber);
            }
        }

        let state = {...this.state};
        state.newArray = numbersArray;
        this.setState(state);
        numbersArray.sort((a, b) => {
            return a - b;
        })
    };

    render() {
        return (
            <div className="App">
                <div>
                    <button className="btn" onClick={this.getNumbers}>New numbers</button>
                </div>
                <Numbers numberOne={this.state.newArray[0]}
                         numberTwo={this.state.newArray[1]}
                         numberThree={this.state.newArray[2]}
                         numberFour={this.state.newArray[3]}
                         numberFive={this.state.newArray[4]}
                />
            </div>
        );
    };
}

export default App;
