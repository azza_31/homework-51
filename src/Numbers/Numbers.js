import React from 'react';

const Numbers = props => {
    return (
        <div className="numbers">
            <ul className="list">
                <li>{props.numberOne}</li>
                <li>{props.numberTwo}</li>
                <li>{props.numberThree}</li>
                <li>{props.numberFour}</li>
                <li>{props.numberFive}</li>
            </ul>
        </div>
    );
};

export default Numbers;